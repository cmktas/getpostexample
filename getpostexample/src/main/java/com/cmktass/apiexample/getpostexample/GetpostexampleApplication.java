package com.cmktass.apiexample.getpostexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetpostexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetpostexampleApplication.class, args);
	}

}
