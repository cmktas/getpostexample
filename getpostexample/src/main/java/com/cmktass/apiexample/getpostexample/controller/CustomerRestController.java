package com.cmktass.apiexample.getpostexample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cmktass.apiexample.getpostexample.dao.CustomerDAO;
import com.cmktass.apiexample.getpostexample.entity.Customer;

@RestController
@RequestMapping("/deneme")
public class CustomerRestController {

	public CustomerDAO customerDAO;
	
	@Autowired
	public CustomerRestController(CustomerDAO customerDAO) {
		
		this.customerDAO=customerDAO;
		
	}
	
	@GetMapping("/customer/{id}")
	public Customer getCustomer1(@PathVariable int id) {
		
		Customer customer=customerDAO.getCustomer(id);
		if(customer==null) {
			throw new RuntimeException("İstenilen id'de musteri bulunamadi.");
		}
		return customer;
	}
	
	@PostMapping("/customer")
	public void getCustomer(@RequestBody Customer customer) {
		
		customerDAO.addCustomer(customer);
		
	}
	
	
}
