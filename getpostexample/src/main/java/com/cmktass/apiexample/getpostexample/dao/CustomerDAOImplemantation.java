package com.cmktass.apiexample.getpostexample.dao;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cmktass.apiexample.getpostexample.entity.Customer;

@Repository
public class CustomerDAOImplemantation implements CustomerDAO{

	
	private EntityManager entityManager;
	
	@Autowired
	public CustomerDAOImplemantation(EntityManager entityManager) {
		
		this.entityManager=entityManager;
		
	}
	
	@Override
	public Customer getCustomer(int id) {
		Session currentSession=entityManager.unwrap(Session.class);
		return currentSession.get(Customer.class, id);
	}

	@Override
	public boolean addCustomer(Customer customer) {
		Session currentSession=entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(customer);
		return true;
		
	}

}
