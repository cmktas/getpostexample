package com.cmktass.apiexample.getpostexample.dao;

import com.cmktass.apiexample.getpostexample.entity.Customer;

public interface CustomerDAO {

	public Customer getCustomer(int id);
	
	public boolean addCustomer(Customer customer);
	
}
