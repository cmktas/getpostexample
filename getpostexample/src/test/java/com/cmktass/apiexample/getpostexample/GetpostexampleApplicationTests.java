package com.cmktass.apiexample.getpostexample;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.cmktass.apiexample.getpostexample.dao.CustomerDAO;
import com.cmktass.apiexample.getpostexample.entity.Customer;

@SpringBootTest
class GetpostexampleApplicationTests {

	@Autowired
	private CustomerDAO customerDAO;
	
	
	@Test
	@Transactional
	public void testPost() {
		Customer customer=new Customer();
		customer.setId(1);
		customer.setName("ali");
		customer.setPhone("535");
		customer.setAddress("madrid");
		customer.setTckn("345");
		boolean isTrue=customerDAO.addCustomer(customer);
		assertEquals(isTrue,true);
	}
	@Test
    @Transactional
	public void testGet() {
		Customer customer=new Customer();
		customer.setId(1);
		customer.setName("ali");
		customer.setPhone("535");
		customer.setAddress("madrid");
		customer.setTckn("345");
		customerDAO.addCustomer(customer);
		Customer customer1=customerDAO.getCustomer(1);
		assertEquals(customer1.getName(),customer.getName());
		assertEquals(customer1.getAddress(),customer.getAddress());
	}

}
